<?php
/*
Plugin Name: MXO - Emplois
Plugin URI: http://www.mxo.agency
Description: Module d'emplois
Version: 1.0
Author: Dee
Author URI: andre.noel@agencemxo.com
License: MXO Copyright, exclusive usage. 
*/



/*-------------------------------------------------------------------------------------------*/
/* cpt_emploi Post Type */
/*-------------------------------------------------------------------------------------------*/
class cpt_emploi {
	
	function cpt_emploi() {
		add_action('init',array($this,'create_post_type'));
	}
	
	function create_post_type() {
		$labels = array(
			'name'					=> 'Emplois',
			'singular_name'			=> 'Emploi',
			'add_new'				=> 'Ajouter un emploi',
			'all_items'				=> 'Tous les emplois',
			'add_new_item'			=> 'Ajouter un emploi',
			'edit_item'				=> 'Modifier l&apos;emploi',
			'new_item'				=> 'Ajouter un emploi',
			'view_item'				=> 'Voir l&apos;emploi',
			'search_items'			=> 'Rechercher un emploi',
			'not_found'				=> 'Aucun emploi trouv&eacute;',
			'not_found_in_trash'    => 'Aucune emploi dans la corbeille',
			'menu_name'				=> 'Emplois'

		);
		$args = array(
			'labels' 				=> $labels,
			'description' 			=> "",
			'public' 				=> true,
			'exclude_from_search' 	=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true, 
			'show_in_nav_menus' 	=> true, 
			'show_in_menu' 			=> true,
			'show_in_admin_bar' 	=> true,
			'menu_position' 		=> 5,
			'menu_icon' 			=> 'dashicons-groups',
			'capability_type' 		=> 'page',
			'hierarchical' 			=> true,
			'supports' 				=> array('title','editor','excerpt'),
			'has_archive' 			=> true,
			'rewrite' 				=> array('slug' => 'emplois'),
			'query_var' 			=> true,
			'can_export' 			=> true
		); 
		register_post_type('cpt_emploi',$args);
	}
}

$cpt_emploi = new cpt_emploi();


// on ajoute les titres des emplois - anglais
add_filter("gform_pre_render_3", "populate_emplois_en");
function populate_emplois_en($form){

	$pages = get_pages(array('post_type' => 'cpt_emploi', 'post_status' => 'publish', 'sort_order' => 'ASC', 'sort_column' => 'menu_order'));

	//Creating drop down item array.
	$items = array();

	//Adding initial blank value.
	$items[] = array("text" => "--- Select ---", "value" => "");

	//Adding post titles to the items array
	foreach($pages as $page)
		$items[] = array("value" => $page->post_name, "text" => $page->post_title, "isSelected" => $page->post_name == get_post()->post_name);

	//Adding items to field id 8. Replace 8 with your actual field id. You can get the field id by looking at the input name in the markup.
	foreach($form["fields"] as &$field)
		if($field["id"] == 6){            
			$field["choices"] = $items;
			$field['defaultValue'] = $page->post_name;
		}
	

	return $form;
}

// on ajoute les titres des emplois - francais
add_filter("gform_pre_render_4", "populate_emplois_fr");
function populate_emplois_fr($form){

	$pages = get_pages(array('post_type' => 'cpt_emploi', 'post_status' => 'publish', 'sort_order' => 'ASC', 'sort_column' => 'menu_order'));

	//Creating drop down item array.
	$items = array();

	//Adding initial blank value.
	$items[] = array("text" => "--- Choisissez ---", "value" => "");

	//Adding post titles to the items array
	foreach($pages as $page) {
		$items[] = array("value" => $page->post_name, "text" => $page->post_title, "isSelected" => $page->post_name == get_post()->post_name);
	}

	//Adding items to field id 8. Replace 8 with your actual field id. You can get the field id by looking at the input name in the markup.
	foreach($form["fields"] as &$field)
		if($field["id"] == 6){         
			$field["choices"] = $items;
		}

	return $form;
}