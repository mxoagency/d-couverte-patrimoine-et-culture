<?php
/*
Plugin Name: MXO - E-rep
Plugin URI: http://www.mxo.agency
Description: Shortcode pour formulaire e-rep
Version: 1.0
Author: Seb.R.
Author URI: sebastien.rochon@agencemxo.com
License: MXO Copyright, exclusive usage. 
*/
 
// create custom plugin settings menu
add_action('admin_menu', 'erep_create_menu');

function erep_create_menu() {

	//create new top-level menu
	add_menu_page('E-rep Settings', 'E-rep Settings', 'administrator', __FILE__, 'erep_settings_page'  );

	//call register settings function
	add_action( 'admin_init', 'register_erep_settings' );
}


function register_erep_settings() {
	//register our settings
	register_setting( 'erep-plugin-settings-group', 'account_ID' );
	$langs = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
	foreach($langs as $lang){
		register_setting( 'erep-plugin-settings-group', 'groupe_ID_'.$lang['language_code'] );
		register_setting( 'erep-plugin-settings-group', 'conf_msg_'.$lang['language_code'] );
		register_setting( 'erep-plugin-settings-group', 'email_label_'.$lang['language_code'] );
		register_setting( 'erep-plugin-settings-group', 'email_placeholder_'.$lang['language_code'] );
		register_setting( 'erep-plugin-settings-group', 'submit_label_'.$lang['language_code'] );
	}
}

function erep_settings_page() {
?>
<div class="wrap">
<h2>E-rep</h2>

<form method="post">
    <?php 
    	settings_fields( 'erep-plugin-settings-group' );    
    	do_settings_sections( 'erep-plugin-settings-group' ); 
    	if (isset($_POST['account_ID'])) { update_option('account_ID', $_POST['account_ID']); }
	    if (isset($_POST['groupe_ID_'.ICL_LANGUAGE_CODE])) { update_option('groupe_ID_'.ICL_LANGUAGE_CODE, $_POST['groupe_ID_'.ICL_LANGUAGE_CODE]); }
	    if (isset($_POST['conf_msg_'.ICL_LANGUAGE_CODE])) { update_option('conf_msg_'.ICL_LANGUAGE_CODE, $_POST['conf_msg_'.ICL_LANGUAGE_CODE]); }
	    if (isset($_POST['email_label_'.ICL_LANGUAGE_CODE])) { update_option('email_label_'.ICL_LANGUAGE_CODE, $_POST['email_label_'.ICL_LANGUAGE_CODE]); }
	    if (isset($_POST['email_placeholder_'.ICL_LANGUAGE_CODE])) { update_option('email_placeholder_'.ICL_LANGUAGE_CODE, $_POST['email_placeholder_'.ICL_LANGUAGE_CODE]); }
	    if (isset($_POST['submit_label_'.ICL_LANGUAGE_CODE])) { update_option('submit_label_'.ICL_LANGUAGE_CODE, $_POST['submit_label_'.ICL_LANGUAGE_CODE]); }
    ?>
    <table class="form-table">
		<colgroup>
			<col width="20%">
			<col width="80%">
		</colgroup>
        <tr valign="top">
        <th scope="row"><label for="account_ID">Account ID</label></th>
        <td><input type="text" name="account_ID" value="<?php echo esc_attr( get_option('account_ID') ); ?>" /></td>
        </tr>

    	<tr valign="top">
        <th scope="row"><label for="groupe_ID_<?= ICL_LANGUAGE_CODE; ?>">Groupe ID</label></th>
        <td><input type="text" name="groupe_ID_<?= ICL_LANGUAGE_CODE; ?>" value="<?php echo esc_attr( get_option('groupe_ID_'.ICL_LANGUAGE_CODE) ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><label for="conf_msg_<?= ICL_LANGUAGE_CODE; ?>">Confirmation Message</label></th>
        <td><textarea name="conf_msg_<?= ICL_LANGUAGE_CODE; ?>"><?php echo esc_attr( get_option('conf_msg_'.ICL_LANGUAGE_CODE) ); ?></textarea></td>
        </tr>

        <tr valign="top">
        <th scope="row"><label for="email_label_<?= ICL_LANGUAGE_CODE; ?>">Email field label</label></th>
        <td><input type="text" name="email_label_<?= ICL_LANGUAGE_CODE; ?>" value="<?php echo esc_attr( get_option('email_label_'.ICL_LANGUAGE_CODE) ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><label for="email_placeholder_<?= ICL_LANGUAGE_CODE; ?>">Email placeholder label</label></th>
        <td><input type="text" name="email_placeholder_<?= ICL_LANGUAGE_CODE; ?>" value="<?php echo esc_attr( get_option('email_placeholder_'.ICL_LANGUAGE_CODE) ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><label for="submit_label_<?= ICL_LANGUAGE_CODE; ?>">Submit bouton label</label></th>
        <td><input type="text" name="submit_label_<?= ICL_LANGUAGE_CODE; ?>" value="<?php echo esc_attr( get_option('submit_label_'.ICL_LANGUAGE_CODE) ); ?>" /></td>
        </tr>
    </table>
    	
    <style>
    	input[type="text"],textarea{ width: 100%; max-width: 500px; }
    	textarea{ height: 100px; }
    </style>

    <?php submit_button(); ?>

</form>
</div>
<?php }


function get_erep_form($atts){
	ob_start();

	global $post;
?>

	<form id="form_newsletter" class="reg_form" action="http://app.e-rep.ca/optin" method="post" accept-charset="utf-8" autocomplete="off">
		<fieldset>												
			<div class="input_wrap">
				<label for="ci_email"><?php echo esc_attr( get_option('email_label_'.ICL_LANGUAGE_CODE) ); ?></label>
				<input type="text" id="ci_email" name="ci_email" maxlength="255" placeholder="<?php echo esc_attr( get_option('email_placeholder_'.ICL_LANGUAGE_CODE) ); ?>" />
			</div>
			<div style="display:block; visibility:hidden; height:1px;">
				<input style="display:none;" type="text" id="ci_verification" name="ci_verification" />
				<input type="hidden" id="ci_groups" name="ci_groups" value="<?php echo esc_attr( get_option('groupe_ID_'.ICL_LANGUAGE_CODE) ); ?>" />
				<input type="hidden" id="ci_account" name="ci_account" value="<?php echo esc_attr( get_option('account_ID') ); ?>" />
				<?php if(ICL_LANGUAGE_CODE == "en"){ ?>
					<input type="hidden" id="ci_language" name="ci_language" value="en_ca" />
				<?php } elseif(ICL_LANGUAGE_CODE == "fr") { ?>
					<input type="hidden" id="ci_language" name="ci_language" value="fr_ca" />
				<?php } ?>
				<input type="hidden" id="ci_sent_url" name="ci_sent_url" value="" />
				<input type="hidden" id="ci_error_url" name="ci_error_url" value="" />
				<input type="hidden" id="ci_confirm_url" name="ci_confirm_url" value="" />
			</div>
			<div id="message_newsletter" class="hidden"><p><?php echo esc_attr( get_option('conf_msg_'.ICL_LANGUAGE_CODE) ); ?></p></div>
			<div id="message_newsletter_empty_email" class="hidden"><?php echo __('Veuillez entrer votre adresse courriel. <br />','mxo_erep'); ?></div>
			<div id="message_newsletter_validate_email" class="hidden"><?php echo __('Veuillez entrer une adresse courriel valide. <br />','mxo_erep'); ?></div>
			<input type="submit" value="<?php echo esc_attr( get_option('submit_label_'.ICL_LANGUAGE_CODE) ); ?>" />
		</fieldset>
	</form>
	<script type="text/javascript">
		jQuery(document).ready(function() {

		jQuery("#form_newsletter").submit(function(event) {

			jQuery("#message_newsletter_empty_email").addClass("hidden");
			jQuery("#message_newsletter_validate_email").addClass("hidden");
			jQuery("#message_newsletter").addClass("hidden");
			
			event.preventDefault();
			
			if( validateEmail( jQuery("#ci_email").val() ) ){

				jQuery.ajax({
					url: "http://app.e-rep.ca/optin",
					type: "POST",
					crossDomain: true,
					data: { 
						ci_groups : jQuery("#ci_groups").val(), 
						ci_language : jQuery("#ci_language").val(), 
						ci_email : jQuery("#ci_email").val(),
						ci_account : jQuery("#ci_account").val(),
						ci_sent_url : jQuery("#ci_sent_url").val(),
						ci_error_url : jQuery("#ci_error_url").val(),
						ci_confirm_url : jQuery("#ci_confirm_url").val()
					},
					dataType: 'json'
				});

				jQuery("#form_newsletter .input_wrap, #form_newsletter input[type='submit']").hide();
				jQuery("#message_newsletter").removeClass("hidden");

			} else if( jQuery("#ci_email").val().length == 0 ){

				jQuery("#message_newsletter_empty_email").removeClass("hidden");


			} else {

				jQuery("#message_newsletter_validate_email").removeClass("hidden");
			}

		});

	});

	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	</script>
	
	<?php
	wp_reset_postdata();

	return ob_get_clean();
}

add_shortcode('erep_form', 'get_erep_form');

?>