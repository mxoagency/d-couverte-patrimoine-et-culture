<?php
/*
Plugin Name: MXO - config, functions
Plugin URI: http://www.mxo.agency
Description: Core stuff for administration by MXO.
Version: 1.0
Author: hAPy & Dee
Author URI: andre.noel@agencemxo.com
License: MXO Copyright, exclusive usage. 
*/



// User admins
$g_uID = array(1,2);

function sf_admin_head() {
	$blog_url = get_bloginfo('url');
	$templ_url = get_bloginfo('template_url');
	
	// on cache les versions de WordPress ainsi que les mises � jour disponible
	global $g_uID;
	if ( !in_array(get_current_user_id(), $g_uID) ) {
		echo '
			<style type="text/css">
				#user-1,
				.subsubsub .administrator,
				#wp-version-message,
				.versions p,
				.versions br,
				#dashboard-widgets-wrap .b-tags, #dashboard-widgets-wrap .tags,
				#dashboard_right_now .table_discussion,
				/*#screen-meta-links,*/
				#wpfooter,
				#rg_forms_dashboard .inside .textright,
				#adminmenu .update-plugins
				{
					display:none !important;
				}
			</style>';
	}
	
}
add_action('admin_head', 'sf_admin_head');

// pour enlever la bar d'amin � partir de la version de wordpress 3.3
function remove_admin_bar_links() {
	global $g_uID;
	if ( !in_array(get_current_user_id(), $g_uID) ) {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('wp-logo');
		$wp_admin_bar->remove_menu('view-site');
		$wp_admin_bar->remove_menu('new-content');
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('wpseo-menu');
		// pour ajouter un item dans la bar d'amin
		//$wp_admin_bar->add_node($arg);
	}
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );


// enl�ve les widgets dans le dashboard
function remove_dashboard_widgets() {
	global $g_uID;
	if ( !in_array(get_current_user_id(), $g_uID) ) {
		global $wp_meta_boxes;
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		//unset($wp_meta_boxes['dashboard']['normal']['core']['icl_dashboard_widget']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		//unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	}
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');




// CSS custom pour le login
function my_custom_login_logo() { echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/../../plugins/mxo-core/css/login.css" />'; }
add_action('login_head', 'my_custom_login_logo');

// Remplace le lien du bouton MXO
function the_url( $url ) { return $url = 'http://www.mxo.agency'; }
add_filter( 'login_headerurl', 'the_url' );

// Remplace le titre du bouton MXO
function login_title( $logo_title ) { return $logo_title = 'MXO | agence totale'; }
add_filter( 'login_headertitle', 'login_title' );

// Background color random pour la page du login
function login_classes( $classes ) { $random_bg_color = rand(1, 3); $classes[] = 'bg'.$random_bg_color; return $classes; }
add_filter( 'login_body_class', 'login_classes' );





// desactive la toolbar dans le front-end
//show_admin_bar(false);


// enl�ve la boite � Format � qui contient diff�rent type de post ( aside, link, image, gallery, Status, Quote,... )
function remove_post_formats() { remove_theme_support('post-formats'); }
add_action ('after_setup_theme', 'remove_post_formats', 11);



// PLUGINS //////////////////////////////////////////////////////////////

// WPML - fonction pour lister les langues dans le menu
/**/
function languages_list_header(){
	$languages = icl_get_languages('skip_missing=0&orderby=code');
	
	if (!empty($languages)) {
		foreach ($languages as $l) {
			if (!$l['active']) {
				echo '<a href="'.$l['url'].'" style="background-image:none;"><span class="shadow" style="cursor:pointer;">';
				echo icl_disp_language($l['translated_name'], $l['native_name'], true);
				echo '</span></a>';
			}
		}
	}
}


// ajoute la langue comme classe dans le BODY
function extend_body_class($c) {
	$icl_lang = ICL_LANGUAGE_CODE;
	if ($icl_lang == "ICL_LANGUAGE_CODE"):
		$icl_lang = "fr";
	endif;
	
	$c[] = 'lang-' . $icl_lang;
	return $c;
}
add_filter('body_class', 'extend_body_class');



// permet de cacher l'Administrateur de la liste des r�les
class JPB_User_Caps {

  // Add our filters
  function JPB_User_Caps(){
    add_filter( 'editable_roles', array(&$this, 'editable_roles'));
    add_filter( 'map_meta_cap', array(&$this, 'map_meta_cap'),10,4);
  }

  // Remove 'Administrator' from the list of roles if the current user is not an admin
  function editable_roles( $roles ){
    if( isset( $roles['administrator'] ) && !current_user_can('administrator') ){
      unset( $roles['administrator']);
    }
    return $roles;
  }

  // If someone is trying to edit or delete and admin and that user isn't an admin, don't allow it
  function map_meta_cap( $caps, $cap, $user_id, $args ){

    switch( $cap ){
        case 'edit_user':
        case 'remove_user':
        case 'promote_user':
            if( isset($args[0]) && $args[0] == $user_id )
                break;
            elseif( !isset($args[0]) )
                $caps[] = 'do_not_allow';
            $other = new WP_User( absint($args[0]) );
            if( $other->has_cap( 'administrator' ) ){
                if(!current_user_can('administrator')){
                    $caps[] = 'do_not_allow';
                }
            }
            break;
        case 'delete_user':
        case 'delete_users':
            if( !isset($args[0]) )
                break;
            $other = new WP_User( absint($args[0]) );
            if( $other->has_cap( 'administrator' ) ){
                if(!current_user_can('administrator')){
                    $caps[] = 'do_not_allow';
                }
            }
            break;
        default:
            break;
    }
    return $caps;
  }

}

$jpb_user_caps = new JPB_User_Caps();



// Add Custom Post Type to WP-ADMIN Right Now Widget
// Ref Link: http://wpsnipp.com/index.php/functions-php/include-custom-post-types-in-right-now-admin-dashboard-widget/
// http://wordpress.org/support/topic/dashboard-at-a-glance-custom-post-types
// http://halfelf.org/2012/my-custom-posttypes-live-in-mu/
function vm_right_now_content_table_end() {
	$args = array(
		'public' => true ,
		'_builtin' => false
	);
	$output = 'object';
	$operator = 'and';
	$post_types = get_post_types( $args , $output , $operator );
	foreach( $post_types as $post_type ) {
		$num_posts = wp_count_posts( $post_type->name );
		$num = number_format_i18n( $num_posts->publish );
		$text = _n( $post_type->labels->singular_name, $post_type->labels->name , intval( $num_posts->publish ) );
		if ( current_user_can( 'edit_posts' ) ) {
			$cpt_name = $post_type->name;
		}
		echo '<li class="'.$cpt_name.'-count"><tr><a href="edit.php?post_type='.$cpt_name.'"><td class="first b b-' . $post_type->name . '"></td>' . $num . ' <td class="t ' . $post_type->name . '">' . $text . '</td></a></tr></li>';
	}
	$taxonomies = get_taxonomies( $args , $output , $operator );
	foreach( $taxonomies as $taxonomy ) {
		$num_terms  = wp_count_terms( $taxonomy->name );
		$num = number_format_i18n( $num_terms );
		$text = _n( $taxonomy->labels->name, $taxonomy->labels->name , intval( $num_terms ));
		if ( current_user_can( 'manage_categories' ) ) {
			$cpt_tax = $taxonomy->name;
		}
		echo '<li class="post-count"><tr><a href="edit-tags.php?taxonomy='.$cpt_tax.'"><td class="first b b-' . $taxonomy->name . '"></td>' . $num . ' <td class="t ' . $taxonomy->name . '">' . $text . '</td></a></tr></li>';
	}
}
add_action( 'dashboard_glance_items' , 'vm_right_now_content_table_end' );




// tinyMCE - ajoute un CSS � la toolbar
/* if ( ! function_exists('tdav_css') ) {
	function tdav_css($wp) {
		$wp .= ',' . get_bloginfo('stylesheet_directory') . '/includes/css/tinyMCE.css';
		return $wp;
	}
}
add_filter( 'mce_css', 'tdav_css' ); */


// ne met pas de lien par d�faut sur les images upload�
update_option('image_default_link_type','none');



// enl�ve les accents quand on upload un documents/images
/* cette version semble ne plus fonctionner sur wp3.9.1 ?
function sanitize_filename_on_upload ($filename) {
	return remove_accents( $filename );
}
add_filter('sanitize_file_name', 'sanitize_filename_on_upload', 10);
*/
add_filter('sanitize_file_name', 'remove_accents' );


// 'Search Engines Blocked' Highlighter
function rbd_search_engines_blocked() {
echo '<style type="text/css">
	#dashboard-widgets a[href="options-reading.php"] { font-weight: bold; color: #FFFFFF; background-color: #CC0000; display: inline-block; padding: 15px 20px; border-radius: 5px; font-size:24px; margin-top: 10px; }
	#gform_notification_to_email{ width: 300px; }
</style>'; };
add_action('admin_head', 'rbd_search_engines_blocked');




// pour afficher le nom/permalink de la section � anc�tre �
if(!function_exists('get_post_top_ancestor_id')){
	/* Gets the id of the topmost ancestor of the current page. Returns the current page's id if there is no parent. */
	function get_post_top_ancestor_id(){
		global $post;
		
		if($post->post_parent){
			$ancestors = array_reverse(get_post_ancestors($post->ID));
			return $ancestors[0];
		}
		
		return $post->ID;
	}
}


// ajoute l'ancetre comme classe dans le BODY
function extend_body_class_ancestor($g) {
	$ancestors_id = get_post_top_ancestor_id();
	$g[] = 'ancestor-' . $ancestors_id;

	return $g;
}
add_filter('body_class', 'extend_body_class_ancestor', 10, 3);


// n'affiche pas les pages en brouillon dans Apparence > Menu
function exclude_draft_nav_items( $items, $menu, $args )
{
	global $wpdb;

	//add your custom posttypes to this array
	$allowed_posttypes = array( 'post', 'page' );

	$sql = "SELECT ID FROM {$wpdb->prefix}posts WHERE (post_status='draft' OR post_status='pending') AND ID=%d && post_type=%s";

	foreach ( $items as $k => $item )
	{
		if( in_array($item->object, $allowed_posttypes) )
		{
			$query = $wpdb->prepare( $sql, $item->object_id, $item->object );
			$result = $wpdb->get_var( $query );

			if( $result ) unset($items[$k]);
		}
	}

	return $items;
}
add_filter( 'wp_get_nav_menu_items', 'exclude_draft_nav_items', 10, 3 );