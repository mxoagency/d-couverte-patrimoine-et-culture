<?php 
/* Template Accueil */
get_header();
?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.btn-map').click(function (event) {
			jQuery.cookie('defaultInfoWindow', $(this).data('name'), { path: '/' });
		});
	});
</script>

<section class="container-carrousel">
<?php if( have_rows('carrousel') ): ?>
	<div class="carrousel">
	<?php while ( have_rows('carrousel') ) : the_row(); 
		$image = get_sub_field('image_carrousel');
		if( !empty($image) ): 
	?>
		<div class="slide" style="background: url(<?php echo $image['sizes']['carrousel-home']; ?>) center center no-repeat;background-size: cover;">
			<div class="container"> 
				<?php if (get_sub_field('titre_carrousel') || get_sub_field('texte_carrousel')): ?>
					<div class="carrousel-content">
						<h2><?php the_sub_field('titre_carrousel'); ?></h2>
						<?php the_sub_field('texte_carrousel'); ?>
						<?php if (get_sub_field('lien') && get_sub_field('texte_du_lien')): ?>
							<a class="btn" href="<?php the_sub_field('lien'); ?>"><?php the_sub_field('texte_du_lien'); ?></a>
						<?php endif ?>
					</div>
				<?php endif ?>
			</div>
			<span class="credits">Photo : <?php the_sub_field('credits_carrousel'); ?></span>
		</div>
	<?php endif;
	endwhile; ?>
	</div>
<?php endif; ?>
</section>

<section class="title-home"> 
	<div class="container"> 
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="title-lines">
					<h1><span><?php echo get_the_title();?></span></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'includes/list-types.php'; ?>

<?php $posts = get_field('attraits');

if( $posts ): ?> 
	<section class="square-container">
			<div class="wrap_articles">
				<div class="square-container">
					<div class="row">
						<?php foreach( $posts as $post) :  
							setup_postdata($post); 
							$terms = wp_get_post_terms($post->ID, 'taxo_attraits');
							$primaryTerm = $terms[0];
							$term_slug = $primaryTerm->slug;?>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="square-display matchHeight <?php echo $term_slug; ?>" style="background: url('<?php the_post_thumbnail_url('square-thumb' ); ?>') center center no-repeat;background-size:cover;">
									
									<div class="square-content matchHeight">
										<div class="square-text">
											<h4><?php the_title(); ?></h4>
											<a href="<?php echo get_permalink($post->id); ?>" class="learn_more"><?php echo __('Voir la fiche', 'mxo'); ?></a>
										</div>
										<a href="<?= get_permalink(61); ?>" class="btn-map" data-name="<?= strtolower($post->post_title) ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/template/map-icon.svg" alt=""></a>
									</div>
									<a class="full-click" href="<?php echo get_permalink($post->id); ?>"></a>
								</div>
							</div>
						
						<?php endforeach; wp_reset_postdata(); wp_reset_query(); ?>
					</div>
				</div>
				<div class="container-btn">
					<a class="btn" href="<?php echo get_post_type_archive_link('cpt_attraits'); ?>"><?php echo __('voir tous les attraits', 'mxo') ?></a>
				</div>
			</div>
	</section>
	<?php endif; ?>

<?php get_footer(); ?>