<?php 
get_header();

/**
 * The Template for displaying all single posts.
 */
?>


	<?php 	$terms = wp_get_post_terms($post->ID, 'taxo_attraits');
			$primaryTerm = $terms[0];	?>

	<?php $header = get_field('image_header'); ?>
	<?php if ($header): ?>
		<div class="header-page" style="background: url('<?php echo $header['sizes']['header-page']; ?>') center center no-repeat;background-size:cover;">
		<span class="credits"><?php echo get_field('credits'); ?></span>
			<div class="container">

				<?php 


				//Faire en sorte que le bouton retour mène à la page d'où il est arrivé. S'il venait de l'extérieur du site, amené à la liste d'attraits.
				$url_blog = get_bloginfo('url');

				if (strpos($_SERVER['HTTP_REFERER'], $url_blog) !== false) {
					$url = $_SERVER['HTTP_REFERER'];
				}else{
					$url = get_post_type_archive_link('cpt_attraits');
				}

				?>
				<a class="btn-retour <?php echo $primaryTerm->slug; ?>" href="<?php echo $url ?>">
					<i class="fa fa-angle-left" aria-hidden="true"></i> <?php echo __('Retour', 'mxo')?>
				</a>
			</div>
		</div>
	<?php endif; ?>
	


	<div id="content" class="<?php echo $primaryTerm->slug; ?>" >
		<section> 
			<div class="container"> 
				<div class="row">
					
				 	<aside class="col-xs-12 col-md-4">
				 		<?php include 'sidebar.php'; ?>						
					</aside>
					
					<div class="col-xs-12 col-md-8">
						<h1 class="page_title"><?=get_the_title();?></h1>
						<p class="taxo-name"><?php echo $primaryTerm->name; ?></p>
						<?php if(get_the_content() != '' || get_the_content() != null): ?>
							<div class="wrap_single_article">
								
								<?php while ( have_posts() ) : the_post(); ?>
									<article class="article clearfix" id="article_<?php the_ID() ?>">
										<div class="wysiwyg_content desc">
											<?php the_content(); ?>
										</div>
									</article>  
								<?php endwhile; wp_reset_query(); ?>
							</div>
						<?php else: ?>
							<p><?=__("Il n'y a aucun attrait pour le moment.");?></p>
						<?php endif; ?>

						<?php if (get_field('bibliographie')): ?>
							<div class="biblio">
								<h3><?php echo __('Bibliographie', 'mxo'); ?></h3>
								<?php echo get_field('bibliographie'); ?>
							</div>
						<?php endif ?>

						<?php 

						$images = get_field('galerie');
						if( $images ): ?>
						<div class="galerie">
						    <div class="row">
						    	<div class="grid">
							        <?php 
							        $i=0;
							        foreach( $images as $image ): ?>
							            <div class="grid-item">
							                <a class="photo-gallery" data-index="<?php echo $i ?>" >
							                     <img src="<?php echo $image['sizes']['galerie-thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
							                </a>
							            </div>
							        <?php 
							        $i++;
							        endforeach; ?>
						        </div>
						    </div>
						</div>
						<?php endif; ?>

						<script>
							<?php $imagesGallery = get_field('galerie'); 
							if( $imagesGallery ): ?>
							var items = [ 
						    <?php
						    	$i=0;
							    foreach ($imagesGallery as $img) {
							        if ($i > 0) echo ',';
							        ?>{
							            src: "<?php echo $img['sizes']['large'];?>",
							            w: <?php echo $img['sizes']['large-width'];?>,
							            h: <?php echo $img['sizes']['large-height'];?>,
							            title: "<?php echo $img['caption'];?>"

							        }<?php
							         $i++;
							    }?>
							    ];
							<?php endif; ?>

						    $('.photo-gallery').click(function(){
						        var pswpElement = document.querySelectorAll('.pswp')[0];
						        var options = {
						            index: parseInt($(this).attr('data-index')),
						            captionEl: true,
									zoomEl: true,
									fullscreenEl: true,
									shareEl: false,
									counterEl: true,
									arrowEl: true,
									preloaderEl: true,
						            bgOpacity: 0.95,
						            tapToClose: true,
						            tapToToggleControls: true,
						            mouseUsed: true
						        };

						        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
						        gallery.init();

						    });
					    </script>

					</div>
				</div>
			</div>
		</section>

		<?php include 'includes/attraits-connexes.php'; ?>
	</div>



	<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
	 
<?php get_footer(); ?>