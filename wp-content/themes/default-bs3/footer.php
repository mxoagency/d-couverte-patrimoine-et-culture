<?php
/* The template for displaying the footer. */
?>
		<?php if (!is_page(61)): ?>
			<section class="map-link">
				<div class="container">
					<div class="row">
						<a href="<?php echo get_permalink(icl_object_id(61)); ?>" class="btn btn-carte"><?php echo __('consulter la carte', 'mxo'); ?></a>
					</div>
				</div>
			</section>
		<?php endif ?>

		<footer class="footer">

			<div class="footer-columns">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<div class="newsletter">
								<p><?php echo __('Restez informé avec l\'infolettre de la ville', 'mxo'); ?></p>
								<a href="http://www.stbruno.ca/infolettre" target="_blank" class="btn"><?php echo __('s\'inscrire', 'mxo'); ?></a>
							</div>
							<div class="res-soc">
								<a href="https://www.facebook.com/stbrunodemontarville" target="_blank" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="https://twitter.com/Ville_StBruno" target="_blank" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								<a href="https://www.youtube.com/user/stbrunodemontarville" target="_blank" class="youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a>
							</div>
						</div>
						<div class="nav-footer col-md-2 col-sm-4 col-md-offset-1">
							<p><?php echo __('Navigation', 'mxo'); ?></p>
							<ul>
								<?php wp_nav_menu( array( 'menu' => 'menu_principal_'.ICL_LANGUAGE_CODE, 'items_wrap' => '%3$s', 'container' => '' ) ); ?>
							</ul>
						</div>
						<div class="nav-footer col-md-2 col-sm-4">
							<p><?php echo __('Attraits', 'mxo'); ?></p>
							<ul>
								<?php $terms = get_terms(array(
							    'taxonomy' => 'taxo_attraits',
							    'hide_empty' => true,
								)); ?>
								<?php foreach($terms as $term) : ?>
								<li><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div class="col-md-3 col-sm-4 logo">
							<a href="http://www.stbruno.ca/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/template/logo_st-bruno.png" alt="St-Bruno de-Montarville"></a>
						</div>
					</div>
				</div>
			</div>


			<div id="copyrights_container">
				<div class="container">
					<div class="row">				
						<div id="copyright" class="col-md-8">
							&copy; <?php echo date('Y') . " " .get_bloginfo('name') . ", " . get_bloginfo('description') . __('. Tous droits r&eacute;serv&eacute;s.', 'mxo'); ?> <a href="<?php echo get_permalink(icl_object_id(242)) ?>"><?php echo get_the_title(icl_object_id(242)); ?></a>
						</div>
						<div id="credit_mxo" class="col-md-4">
							<a href="http://mxo.agency" target="_blank" title="MXO | agence totale. Communication, marketing, web et affaires électroniques."><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/template/credit_mxo.png" alt="MXO | agence totale. Communication, marketing, web et affaires électroniques."/></a>
						</div>
						
					</div>
			
					<?php wp_footer(); ?>
					
				</div>
			</div>			
		</footer>
				
	</body>	
</html>