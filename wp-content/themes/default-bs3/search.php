<?php /* The template for displaying Search Results pages */
get_header(); 
?>

	<?php $header = get_field('header_recherche', 'option'); ?>
	<?php if ($header): ?>
		<div class="header-page" style="background: url('<?php echo $header['sizes']['header-page']; ?>') center center no-repeat;background-size:cover;"></div>
	<?php endif; ?>
	
	<div id="content">
		<div class="container"> 
	    	<div class="row-fluid">


			<h1><?php echo __('Recherche ','mxo'); ?></h1>
	    	<?php echo get_field('texte_dexplication_recherche', 'option'); ?>

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="child-item search-item">
						<?php 
						echo '<h2>' . get_the_title() . '</h2>';?>

						<?php //echo "<pre> Label : "; print_r($post); echo "</pre>"; ?>
						<?php //echo 'TPL : '. get_post_meta( $post->ID, '_wp_page_template', true ); ?>

						<?php // verifier si on est dans un template en particulier
						if ( get_post_type() == 'cpt_attraits' ) {
							the_excerpt();
						/*} elseif( get_post_type() == 'cpt_events' ) { 
							echo acf_excerpt('at_description', $post->ID, 45);*/
						} else {
							the_excerpt();
							//the_content();
						}
						?>

						<a class="learn_more" href="<?php the_permalink(); ?>">
							<?php echo (ICL_LANGUAGE_CODE == 'en') ? 'Learn more' : 'En savoir plus' ?>
						</a>
					</div>
				<?php endwhile; ?>
				<div class="pagination"><?php the_pagination(); ?></div> 	
				<?php //theme_pagination(); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					
						<h2 class="entry-title">
							<?php echo (ICL_LANGUAGE_CODE == 'en') ? 'Nothing was found' : 'Rien n&rsquo;a été trouvé' ?>
						</h2>
					

					<div class="entry-content">
						<?php if( ICL_LANGUAGE_CODE == 'en' ){ ?>
							<p>Sorry, but nothing matched your search criteria. Please try again with different keywords.</p>
						<?php } else { ?>
							<p>Désolé, mais rien ne correspond à votre critère de recherche. Veuillez réessayer avec d&rsquo;autres mots-clés.</p>
						<?php } ?>
						
						<?php get_search_form(); ?>
					</div>
				</article>

			<?php 
			endif; 

			wp_reset_postdata();
			wp_reset_query();
			?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>