<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to twentytwelve_comment() which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>

<div id="comments" class="comments-area">

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h2 class="section_title">
			<?php
				printf( _n( 'Un commentaire sur<br/> %2$s', '%1$s commentaires sur %2$s', get_comments_number()),
					number_format_i18n( get_comments_number() ), '<span>&ldquo;' . get_the_title() . '&rdquo;</span>' );
			?>
		</h2>
		<ol class="commentlist">
			<?php wp_list_comments(); ?>
		</ol><!-- .commentlist -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="navigation" role="navigation">
			<h1 class="assistive-text section-heading"><?php _e( 'Navigation des commentaires', 'mxo' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Anciens commentaires', 'mxo' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Nouveaux commentaires &rarr;', 'mxo' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<?php
		/* If there are no comments and comments are closed, let's leave a note.
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="nocomments"><?php _e( 'Désolé, Les commentaires sont fermés.' , 'mxo' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php 
	$fields =  array(
		'author'	=> 	'<p class="comment-form-author"><label for="author">'. __('Nom', 'mxo') .'</label> <span class="required">*</span><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required="required" /></p>',
		'email' 	=> 	'<p class="comment-form-email"><label for="email">'. __('Courriel', 'mxo') .'</label> <span class="required">*</span><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required="required" /></p>',
		'url'   	=> 	'',
	);
	$args = array('fields' => $fields);
	comment_form($args);
	?>

</div><!-- #comments .comments-area -->