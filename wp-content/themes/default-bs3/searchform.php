<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<input type="text" value="" name="s" id="s" placeholder="<?php echo (ICL_LANGUAGE_CODE == 'en') ? 'Search' : 'Rechercher' ?>"  />
	<button type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
</form>