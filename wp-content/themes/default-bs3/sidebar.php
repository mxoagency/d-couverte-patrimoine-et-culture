<?php

	$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'cpt_attraits',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);
	$attraits = get_posts( $args );

	$type_attraits = get_terms(
		'taxo_attraits', array(
		'hide_empty' => false,
	) );

	$markers = array(
		'arts-et-artistes' => 'rouge.png',
		'patrimoine-bati' => 'bleu.png',
		'patrimoine-immateriel' => 'jaune.png',
		'patrimoine-naturel' => 'vert.png',
		'paysages' => 'mauve.png'
	);

	$defaultInfoWindow = get_the_title( $post );;
?>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAL2xxPZcSCNPNbBVHyUci6lhgybNmLLMA"></script>
	<script>
		var openInfoNom = "<?= $defaultInfoWindow ?>";
		var defaultMarker = null;
		var attraits = '[ ';
		var stringAttraits = "";
		<?php foreach ($attraits as $key => &$attrait) :

			$latitude = get_field('latitude',$attrait->ID);
			$longitude = get_field('longitude',$attrait->ID);
			$lien = get_permalink($attrait->ID);
			$thumbnail = get_the_post_thumbnail_url($attrait->ID,'map-thumb');

			$terms = wp_get_post_terms( $attrait->ID, 'taxo_attraits' );
			$categorie = "";
			$term = $terms[0];
			$category = $term->slug;

			$marker = get_template_directory_uri().'/img/template/marqueurs/'.$markers[$category];

			$nom = $attrait->post_title;
			$nom = str_replace("'", "\'", $nom);

			if (!$latitude){
				continue;
			}

			?>
			stringAttraits += <?= '\'{ "latitude":"'.$latitude.'" , "longitude":"'.$longitude.'" , "nom":"'.$nom.'" , "lien":"'.$lien.'" , "thumbnail":"'.$thumbnail.'" , "marker":"'.$marker.'" , "category":"'.$category.'"},\''; ?>

		<?php endforeach; ?>

		stringAttraits = stringAttraits.slice(0,stringAttraits.length-1);
		attraits += stringAttraits + ' ]';
		console.log(attraits);
		attraits = JSON.parse(attraits);
		console.log(attraits);

	</script>

	<script>
		var geoStore;
		var GeoStore;

		jQuery(function($) {

			var GeoStore = (function() {
				//Const
				function GeoStore($mapDom, pins) {
					//Priviliged
					this.map;
					this.infoWindow;
					this.markers = [];
					this.nbXNearest = 4;

					var defaultProperties = {
						$error: undefined,
						$stores: undefined,
						$radius: undefined
					}

					properties = $.extend({}, defaultProperties);

					//Private
					function load() {
						this.map = new google.maps.Map($mapDom.get(0), {
							center: new google.maps.LatLng(47, -73),
							zoom: 6,
							mapTypeId: google.maps.MapTypeId.ROADMAP,
						});

						for (var i = 0; i < pins.length; i++) {
							var pin = pins[i];

							loadMarker.call(this, pin);
						}

						var bounds = new google.maps.LatLngBounds();

						for (var i=0; i<this.markers.length; i++) {
							if(this.markers[i].getVisible()) {
								bounds.extend( this.markers[i].getPosition() );
							}
						}

						this.map.fitBounds(bounds);

						if (openInfoNom){
							checkDefaultPopup.call(this, defaultMarker);
						}

					}

					function checkDefaultPopup(marker) {
						google.maps.event.trigger(marker, 'click');
						jQuery.removeCookie('defaultInfoWindow', { path: '/' });
					}

					function loadMarker(pin) {
						var latLng = new google.maps.LatLng(pin.latitude, pin.longitude);

						marker = new google.maps.Marker({
							map: this.map,
							position: latLng,
							draggable: false,
							icon: pin.marker,
							title: pin.nom
						});

						if (pin.nom.toLowerCase() == openInfoNom.toLowerCase()){
							defaultMarker = marker;
						}

						(function(that, marker, pin) {
							google.maps.event.addListener(marker, 'click', function() {
								if (that.infoWindow) {
									that.infoWindow.close();
								}

								that.infoWindow = new google.maps.InfoWindow({
									content: "<div>" +
									'<img src="'+pin.thumbnail+'"/>'+
									"<p>" + pin.nom + "</p>" +
									'<a href="'+pin.lien+'">Voir la fiche</a>' +
									"<div>"
								});

								that.infoWindow.open(that.map, marker);
							});
						})(this, marker, pin);

						this.markers.push(marker);

						if (properties.$stores) {
							properties.$stores.append("<option value='" + (this.markers.length - 1) + "'>" + pin.title + "</option>");
							properties.$stores.show();
						}
					}

					this.filterByTaxo = function(taxonomy) {

						var geocoder = new google.maps.Geocoder();
						var that = this;

						filterByTaxo.call(this, taxonomy);

					}

					function filterByTaxo(taxonomy) {

						if (this.infoWindow)
							this.infoWindow.close();

						for( i=0;i<this.markers.length; i++ ) {
							this.markers[i].setVisible(false);
						}

						if (taxonomy == "tous"){
							for( i=0;i<this.markers.length; i++ ) {
								this.markers[i].setVisible(true);
							}
						}
						else {
							var filteredPins = new Array();
							for (var i = 0; i < pins.length; i++) {
								var pin = pins[i];
								var point=new google.maps.LatLng(pin.latitude,pin.longitude);

								var include = false;
								if (pin.category.indexOf(taxonomy) >= 0)
									include = true;

								if (include) {
									filteredPins.push(point);
								}
							}

							for(var pin in filteredPins) {

								for( i=0;i<this.markers.length; i++ ) {
									var mlat = this.markers[i].position.lat();
									var mlng = this.markers[i].position.lng();

									if (filteredPins[pin].lat() == mlat && filteredPins[pin].lng() == mlng) {
										this.markers[i].setVisible(true);
									}
								}
							};
						}
					}

					function constructor() {
						var that = this;

						if (properties.$stores) {
							properties.$stores.hide();

							properties.$stores.change(function() {
								var markerNum = jQuery(this).val();
								google.maps.event.trigger(that.markers[markerNum], 'click');
							});
						}

						if (properties.$error) {
							properties.$error.hide();
						}

						google.maps.event.addDomListener(window, 'load', function() {
							load.call(that);
						});
					}

					constructor.call(this);
				}

				//Public

				return GeoStore;
			})();

			//minimified from the client's excel sheet

			var geoStore = new GeoStore($("#map"), attraits);

			$(".type_attrait").click(function(e) {
				e.preventDefault();
				geoStore.filterByTaxo($(this).data('slug'));
				$(".type_attrait").removeClass('active');
				$(this).addClass('active');
			});
		});
	</script>

		<aside class="sidebar">
			<?php

			if( have_rows('blocks') ): 
			    while ( have_rows('blocks') ) : the_row(); ?>
					<div class="block">
						<div class="header-block clearfix">
					        <h3><?php the_sub_field('titre_block'); ?></h3>
				        </div>
				        <div class="content-block">
				        	<?php the_sub_field('texte_block'); ?>
				        </div>
					</div>
			    <?php endwhile; ?>
			<?php endif; ?>

			    <div id="map"></div>
		</aside>