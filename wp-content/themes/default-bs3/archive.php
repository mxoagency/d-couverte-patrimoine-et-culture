<?php
/* Template Name: Liste des attraits */
get_header();
?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.btn-map').click(function (event) {
				jQuery.cookie('defaultInfoWindow', $(this).data('name'), { path: '/' });
			});
		});
	</script>


	<?php $header = get_field('header_attrait', 'option'); ?>
	<?php if ($header): ?>
		<div class="header-page" style="background: url('<?php echo $header['sizes']['header-page']; ?>') center center no-repeat;background-size:cover;"></div>
	<?php endif; ?>

	<section id="content"> 
		<div class="container"> 
	    	<div class="row">
			 	<div class="col-xs-12">
					<h1 class="page_title">
						<?php echo __('Les attraits','mxo'); ?>
					</h1>

					<?php if (get_field('texte_dexplication', 'option')): ?>
					<div class="content-desc">
						<?php the_field('texte_dexplication', 'option'); ?>
					</div>
					<?php endif ?>

				</div>
			</div>
		</div>
	</section>

	<?php include 'includes/list-types.php'; ?>


	<?php
	// Les pages ne marchent pas dans les taxos
	//query_posts($query_string . '&orderby=title&order=ASC&paged&posts_per_page=12');
	if ( have_posts() ) : ?>
	<section class="square-container">
			<div class="wrap_articles">
				<div class="square-container">
					<div class="row">
						<?php while (have_posts()) : the_post();  
							$terms = wp_get_post_terms($post->ID, 'taxo_attraits');
							$primaryTerm = $terms[0];
							$term_slug = $primaryTerm->slug;?>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="square-display matchHeight <?php echo $term_slug; ?>" style="background: url('<?php the_post_thumbnail_url('square-thumb' ); ?>') center center no-repeat;background-size:cover;">
									
									<div class="square-content matchHeight">
										<div class="square-text">
											<h4><?php the_title(); ?></h4>
											<a href="<?php echo get_permalink($post->id); ?>" class="learn_more"><?php echo __('Voir la fiche', 'mxo'); ?></a>
										</div>
										<a href="<?= get_permalink(61); ?>" class="btn-map" data-name="<?= strtolower($post->post_title) ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/template/map-icon.svg" alt=""></a>
									</div>
									<a class="full-click" href="<?php echo get_permalink($post->id); ?>"></a>
								</div>
							</div>
						
						<?php endwhile; wp_reset_postdata(); wp_reset_query(); ?>
						<div class="pagination"><?php the_pagination(); ?></div> 
					</div>
				</div>
			</div>
	</section>
	<?php else: ?>
		<p><?=__("Il n'y a aucun attraits pour le moment.", "mxo");?></p>
	<?php endif; ?>
	 
<?php get_footer(); ?>