//////////////////////////////////////////////////////////////////////////
// Fix WPML JS errors and prevent further complications //////////////////
var current_language = (document.getElementById('ICL_LANGUAGE_CODE')!=null ? document.getElementById('ICL_LANGUAGE_CODE').value : false);
var icl_home = (document.getElementById('ICL_HOME_URL')!=null ? document.getElementById('ICL_HOME_URL').value : false);
var icl_vars = {"current_language":current_language,"icl_home":icl_home};
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

jQuery(document).ready(function($){	
	
    $('.matchHeight').matchHeight();
    $('.square-content').matchHeight();

    /** MENU MOBILE **/
      $('body').addClass('pushmenu-push');

      $(document).ready(function() {
        $menuLeft = $('.pushmenu-left');
        $nav_list = $('#nav_list');
        
        $nav_list.click(function() {
          $(this).toggleClass('active');
          $('.pushmenu-push').toggleClass('pushmenu-push-toright');
          //$('html').toggleClass('mobile-menu-active');
          $menuLeft.toggleClass('pushmenu-open');
        });
      });

      //Mobile menu management & behavior
      $('.button-menu-mobile i').click(function(){
        $(this).toggleClass('active');
        $('#menu-mobile').stop().slideToggle(340);
      });

      $('#menu-mobile ul li span.after').click(function(){
        $(this).parent('')
          .toggleClass('open')
          .children('.sub-menu')
          .stop().slideToggle(340);
      });

	//SEARCH

    $("#search_slider").click(function() 
    {
        if ($("#search_box").hasClass("on"))
        {
            $("#search_box").removeClass("on");
        }
        else
        {
            $("#search_box").addClass("on");
        }
    });



    //GALERIE ATTRAIT

    $('.grid').imagesLoaded( function(){
         // Have to wait a bit before append images because they may not be loaded yet
         setTimeout(function(){
            $('.grid').masonry({
              itemSelector: '.grid-item',
            });
         },350);
     });


    //Carrousel Home
    $('.carrousel').bxSlider({
		pager: true,
  		nextText: '',
  		prevText: '',
  		infiniteLoop: true
	});

});