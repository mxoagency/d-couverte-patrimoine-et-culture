<?php 
/* Template par défaut */
get_header(); 
?>
	<?php $header = get_field('image_header'); ?>
	<?php if ($header): ?>
		<div class="header-page" style="background: url('<?php echo $header['sizes']['header-page']; ?>') center center no-repeat;background-size:cover;">
			<span class="credits"><?php echo get_field('credits'); ?></span>
		</div>
	<?php endif; ?>

	<div id="content" >
		<section> 
			<div class="container"> 
				<div class="row">
					<?php if( have_rows('blocks') ): ?>
					 	<aside class="col-xs-12 col-md-4">
					 		<?php include 'sidebar.php'; ?>						
						</aside>
					<?php endif ?>
					<div class="col-xs-12 col-md-8">
						<h1 class="page_title"><?=get_the_title();?></h1>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>

		<?php include 'includes/attraits-random.php'; ?>
	</div>
	 
<?php get_footer(); ?>



	

