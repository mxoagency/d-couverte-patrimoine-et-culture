<?php 
/* Template Name: Contact */
get_header(); 
?>

<?php $header = get_field('image_header'); ?>
	<?php if ($header): ?>
		<div class="header-page" style="background: url('<?php echo $header['sizes']['header-page']; ?>') center center no-repeat;background-size:cover;"></div>
	<?php endif; ?>

	<div id="content" >
		<section> 
			<div class="container"> 
				<div class="row">
					<?php if( have_rows('blocks') ): ?>
					 	<aside class="col-xs-12 col-md-4">
					 		<?php include 'sidebar.php'; ?>						
						</aside>
					<?php endif ?>
					<div class="col-xs-12 col-md-8">
						<h1 class="page_title"><?=get_the_title();?></h1>
						<?php the_content(); ?>
						<?php
						if(ICL_LANGUAGE_CODE == "fr"){ gravity_form(1, false,false,false,'',false); } 
						elseif(ICL_LANGUAGE_CODE == "en") { gravity_form(2, false,false,false,'',false); } 
						?>
					</div>
				</div>
			</div>
		</section>
	</div>
	 
<?php get_footer(); ?>