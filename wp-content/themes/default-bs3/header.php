<?php $lang = get_bloginfo('language');?><!DOCTYPE html>
<html lang="<?php echo $lang ?>">
<head>
	<?php /* Metas and config */ ?>
	<meta charset="<?php bloginfo('charset'); ?>"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('|'); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11"/>
	
	<?php /* Be sure there is at least an image showing up in facebook share */ ?>
	<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/share.jpg"/>

	<?php /* Fav icons and mobile icons */ ?>
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/favicon.ico">
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/favicon_16px.png" sizes="16x16" type="image/png">
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/favicon_32px.png" sizes="32x32" type="image/png">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/icon_57.png" sizes="57x57" rel="apple-touch-icon">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/icon_72.png" sizes="72x72" rel="apple-touch-icon">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/icon_114px.png" sizes="114x114" rel="apple-touch-icon">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/img/metas/icon_144px.png" sizes="144x144" rel="apple-touch-icon">
	
	<?php 
		if (is_singular() && get_option('thread_comments')) { wp_enqueue_script('comment-reply'); }
		wp_head(); 
	?>

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>

	<script type='text/javascript'>
	(function (d, t) {
	  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
	  bh.type = 'text/javascript';
	  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=tcsd6zkp7t8f0oduk0pxpa';
	  s.parentNode.insertBefore(bh, s);
	  })(document, 'script');
	</script>

	<?php /* IE8 support */ ?>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K7DLK8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K7DLK8');</script>
<!-- End Google Tag Manager -->

	<nav id="menu-mobile" class="pushmenu pushmenu-left mobile-menu visible-sm visible-xs">
		<ul class="nav navigation">
			<li class="search_box_mobile">
				<div class="search_wrapper"><?php get_search_form(); ?></div>
			</li>
			<?php wp_nav_menu( array( 'menu' => 'menu_principal_'.ICL_LANGUAGE_CODE, 'items_wrap' => '%3$s', 'container' => '' ) ); ?>
		</ul>
	</nav>

	<header id="header" class="sticky">
		<div class="container">
			<div class="row">
				<div class="container-logo col-md-4 col-sm-8 col-xs-8">
					<a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/template/decouverte-patrimoine-et-culture_logo.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					</a>
				</div>

				<nav class="col-md-8 hidden-sm hidden-xs">
					<ul class="nav">
						<?php wp_nav_menu( array( 'menu' => 'menu_principal_'.ICL_LANGUAGE_CODE, 'items_wrap' => '%3$s', 'container' => '' ) ); ?>
						<?php /* ?><li class="mobile-header search"><?php get_search_form(); ?></li><?php */ ?>
						<li id="search_box">
							<div class="search_wrapper" id="search_wrapper">
							<script>
							  (function() {
							    var cx = '007074826408752697731:pdat7njzawq';
							    var gcse = document.createElement('script');
							    gcse.type = 'text/javascript';
							    gcse.async = true;
							    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							        '//cse.google.com/cse.js?cx=' + cx;
							    var s = document.getElementsByTagName('script')[0];
							    s.parentNode.insertBefore(gcse, s);
							  })();
							</script>
							
							<?php get_search_form(); ?></div>
							<button id="search_slider" class="search-toggle"><i class="fa fa-search"></i></button>
						</li>
					</ul>
				</nav>

				<div class="col-sm-4 col-xs-4 visible-sm visible-xs container-mobile-button">
					<a id="nav_list" class="nav-trigger mobile-button" >
						<i class="fa fa-bars"></i>
					</a>
				</div>
			</div>
		</div>			
	</header>
	
	<?php 
		wp_reset_postdata();
		wp_reset_query();
	?>