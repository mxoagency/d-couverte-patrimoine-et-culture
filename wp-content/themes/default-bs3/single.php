<?php 
get_header();

/**
 * The Template for displaying all single posts.
 */
?>

	
	<div class="header-page">
		<a class="back_link" href="<?= get_permalink(icl_object_id(4,'page',true)); ?>">
			<i class="fa fa-arrow-circle-left"></i><?=__('Retour à la liste')?>
		</a>
	</div>


	<div id="content">
		<section> 
			<div class="container"> 
				<div class="row">
				 	<aside class="col-xs-12 col-md-3">
						<?php include 'sidebar.php'; ?>
					</aside>
					<div class="col-xs-12 col-md-9">
						<h1 class="page_title"><?=get_the_title();?></h1>
						<?php if(get_the_content() != '' || get_the_content() != null): ?>
							<div class="wrap_single_article">
								
								<?php while ( have_posts() ) : the_post(); ?>
									<article class="article clearfix" id="article_<?php the_ID() ?>">
										<header class="clearfix">
											<?php
												// GET ARTICLE DATE PARTS
												$dayNum = get_the_time('d');
												$month3letter = get_the_time('F');
												$year = get_the_time('Y');
												$hour = get_the_time('G:i');
											?>
											<time>
												publié le:
												<span class="day"><?=$dayNum;?></span>
												<span class="month"><?=$month3letter;?></span>
												<span class="year"><?=$year;?></span>
												<span class="time">à <?=$hour;?></span>
											</time>
											<div class="title">
												<span class="author"><?=__("Posté par");?> <strong><?php the_author(); ?></strong></span>
											</div>
										</header>
										<div class="wysiwyg_content desc">
											<?php the_content(); ?>
										</div>
										<footer>
											<p class="tags"><i class="fa fa-tags"></i>
												<?=__('Mots clés associés');?> : 
												<?php the_tags('', ', ',''); ?>
											</p>
											<?php /* AddThis tool embedding */ ?>
											<div class="addthis_sharing_toolbox"></div>
											<?php 
											/* Comments will append here */
											comments_template(); 
											?>
										</footer>
									</article>  
								<?php endwhile; wp_reset_query(); ?>
							</div>
						<?php else: ?>
							<p><?=__("Il n'y a aucun article pour le moment.");?></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	</div>
	 
<?php get_footer(); ?>