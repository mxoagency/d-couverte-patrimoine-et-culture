<?php
/* The template for displaying 404 pages. */
get_header(); 
?>

    <?php $header = get_field('header_404', 'option'); ?>
    <?php if ($header): ?>
        <div class="header-page" style="background: url('<?php echo $header['sizes']['header-page']; ?>') center center no-repeat;background-size:cover;"></div>
    <?php endif; ?>

	<div class="container">
    	        
        <div class="row">
            <div class="col-xs-12">
                <h1 class="titre_section">
					<?php echo (ICL_LANGUAGE_CODE == 'fr') ? 'Erreur 404 - Page non trouvée' : 'Error 404 - Page not found'; ?>
				</h1>
            </div>
		</div>

        <div id="contenu">
            <div class="row">
                <div class="col-xs-12">
                   <?php echo get_field('texte_dexplication_404', 'option'); ?>
                </div>
            </div>
		</div>
	
    </div>

<?php get_footer(); ?>