'use strict';
module.exports = function(grunt) {

    require('time-grunt')(grunt);

    var jsFileList = [
        'bower_components/jquery/dist/jquery.js',
        'bower_components/jquery-ui/jquery-ui.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/bxslider-4/src/js/jquery.bxslider.js',
        'bower_components/matchHeight/jquery.matchHeight.js',
        'bower_components/photoswipe/dist/photoswipe-ui-default.js',
        'bower_components/photoswipe/dist/photoswipe.js',
        'bower_components/masonry/dist/masonry.pkgd.js',
        'bower_components/imagesloaded/imagesloaded.js',
        'js/dom_ready.js',
        '../../plugins/sitepress-multilingual-cms/res/js/sitepress.js',
        '../../plugins/sitepress-multilingual-cms/res/js/language-selector.js'
    ];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            build: {
                files: {
                    'css/style.min.css': [
                        'bower_components/bootstrap/less/bootstrap.less',
                        'bower_components/font-awesome/less/font-awesome.less',
                        'bower_components/photoswipe/dist/photoswipe.css',
                        'bower_components/photoswipe/dist/default-skin/default-skin.css',
                        '../../plugins/sitepress-multilingual-cms/res/css/language-selector.css',
                        '../../plugins/wp-google-maps/css/wpgmza_style.css',
                        'less/style.less',
                        'less/responsive.less',
                    ]
                },
                options: {
                    compress: true
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: jsFileList,
                dest: 'js/script.js'
            },
        },
        uglify: {
            options: {
                compress: true,
                mangle: true,
                sourceMap: true
            },
            target: {
                src: 'js/script.js',
                dest: 'js/script.min.js'
            }
        },
        watch: {
            less: {
                files: [
                    'less/*.less'
                ],
                tasks: ['less:build']
            },
            js: {
                files: jsFileList,
                tasks: ['concat']
            },
            uglifyjs: {
                files: [
                    'js/script.js'
                ],
                tasks: ['uglify']
            }
        }
    });

    grunt.registerTask('default', [
        'build'
    ]);

    grunt.registerTask('build', [
        'less:build',
        'uglify'
    ]);

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
};