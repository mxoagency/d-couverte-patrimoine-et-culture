<section class="list-type">
	<div class="container">
		<div class="row">
			<?php $terms = get_terms(array(
		    'taxonomy' => 'taxo_attraits',
		    'hide_empty' => true,
			)); 
			$object = get_queried_object();

			if (is_archive() && !is_tax()) {
				$current = "active";
			}
			?>
			<ul>
				<li><a class="<?php echo $current; ?>" href="<?php echo get_post_type_archive_link('cpt_attraits'); ?>"><?php echo __('Tous', 'mxo') ?></a></li>
				<?php foreach($terms as $term) : ?>
				<li><a class="<?php if ($term->term_id == $object->term_id) { echo 'active '; } echo $term->slug ?>" href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</section>