<section id="attraits_connexes">
	<div class="title-lines">
		<h2><span><?php echo __('Quelques attraits','mxo'); ?></span></h2>
	</div>

	<?php
		$args = array(
			'post_type' 		=> 'cpt_attraits',
			'posts_per_page'    => 3,
			'order'				=> 'ASC',
			'orderby'    		=> 	'rand',
			'pagination'        => false
		);
		$related_query = new WP_Query( $args );
		?>

	<div class="square-container">
		<div class="row">
			<?php while ( $related_query->have_posts() ) : $related_query->the_post(); 
				$terms = wp_get_post_terms($post->ID, 'taxo_attraits');
				$primaryTerm = $terms[0];
				$term_slug = $primaryTerm->slug;
			?>
				
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="square-display matchHeight <?php echo $term_slug; ?>" style="background: url('<?php the_post_thumbnail_url('square-thumb' ); ?>') center center no-repeat;background-size:cover;">
						
						<div class="square-content matchHeight">
							<div class="square-text">
								<h4><?php the_title(); ?></h4>
								<a href="<?php echo get_permalink($post->id); ?>" class="learn_more"><?php echo __('Voir la fiche', 'mxo'); ?></a>
							</div>
							<a href="" class="btn-map"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/template/map-icon.svg" alt=""></a>
						</div>
						<a class="full-click" href="<?php echo get_permalink($post->id); ?>"></a>
					</div>
				</div>
			
			<?php endwhile; ?>
		</div>
	</div>
	<div class="container-btn">
		<a class="btn" href="<?php echo get_post_type_archive_link('cpt_attraits'); ?>"><?php echo __('voir tous les attraits', 'mxo') ?></a>
	</div>
</section>
