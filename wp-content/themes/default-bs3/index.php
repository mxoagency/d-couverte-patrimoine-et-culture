<?php 
/* Archive */
get_header();
?>
	<section> 
		<div class="container"> 
	    	<div class="row">
			 	<div class="col-xs-12">
					<h1 class="page_title">
						<?php
							if ( is_category() ) {
								echo __('Blogue | Catégorie','mxo') . ' : ' . single_cat_title();
							} elseif ( is_date() ){
								echo __('Blogue | Archives','mxo') . ' : ' . single_month_title(' ', true);
							} elseif( is_tag() ){
								echo __('Blogue | Mot-clé','mxo') . ' : ' . single_tag_title();
							} else {
								echo get_the_title(icl_object_id(4, 'page', false));
							}
						?>
					</h1>
					<?php if(is_category() || is_date() || is_tag()): ?>
						<a class="back_link" href="<?= get_permalink(icl_object_id(4,'page',true)); ?>">
							<i class="fa fa-caret-left"></i><?= __('View all news','mxo'); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
			 	<div class="col-xs-12 col-sm-3">
					<?php include 'sidebar-blog.php'; ?>
				</div>
				<div class="col-xs-12 col-sm-9">
					<?php if ( have_posts() ) : ?>
						<div class="wrap_articles">
							<?php while ( have_posts() ) : the_post(); ?>
								<article class="article clearfix" id="article_<?php the_ID() ?>">
									<?php if (has_post_thumbnail(get_the_ID())): ?>
									<div class="row">
										<div class="col-xs-12 col-sm-4">
											<?=the_post_thumbnail(array(270, 270));?>
										</div>
										<div class="col-xs-12 col-sm-8">
									<?php endif ?>
											<header class="clearfix">
												<?php
													// GET ARTICLE DATE PARTS
													$dayNum = get_the_time('d');
													$month3letter = get_the_time('F');
													$year = get_the_time('Y');
													$hour = get_the_time('G:i');
												?>
												<time>
													publié le:
													<span class="day"><?=$dayNum;?></span>
													<span class="month"><?=$month3letter;?></span>
													<span class="year"><?=$year;?></span>
													<span class="time">à <?=$hour;?></span>
												</time>
												<div class="title">
													<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
													<span class="author"><?=__("Posté par");?> <strong><?php the_author(); ?></strong> <?= __('à', 'mxo') . ' ' . $hour ;?>.</span>
												</div>
											</header>
											<div class="desc">
												<p class="short_desc"><?= get_the_excerpt() ;?></p>
												<a class="cta_links" href="<?php the_permalink(); ?>"><?=__("En savoir plus", "mxo");?></a>
											</div>
									<?php if (has_post_thumbnail(get_the_ID())):?>
										</div><!-- #/ end (conditionnal to a picture uploaded) -->
									</div>
									<?php endif ;?>
								</article>  
							<?php endwhile; wp_reset_query(); ?>
						</div>
					<?php else: ?>
						<p><?=__("Il n'y a aucun article pour le moment.", "mxo");?></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	 
<?php get_footer(); ?>