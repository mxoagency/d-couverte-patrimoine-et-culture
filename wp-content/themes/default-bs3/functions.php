<?php

// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator'); // version de WP
remove_action('wp_head', array($sitepress, 'meta_generator_tag') ); // version de WPML
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

// Remove language selector CSS and JS
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS',true);
define('ICL_DONT_LOAD_LANGUAGES_JS',true);

// Disable emoji
function disable_wp_emojicons() {
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	// add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

// Remove yoast ld output from header
add_filter( 'wpseo_json_ld_output', 'swp_remove_jsonld_yoast', 10, 2 );
function swp_remove_jsonld_yoast($data, $context){
	if($data['@type'] == 'WebSite'){
		$data = false;
	}
	return $data;
}

add_action( 'wp_print_styles', 'deregister_wpgmaps_style', 100 );
function deregister_wpgmaps_style() {
	wp_deregister_style( 'wpgmaps-style' );
}

add_action('init','clean_header');
function clean_header(){
	wp_deregister_script( 'comment-reply' );
}

// CSS dans l'admin
function customAdmin() {

	echo '<style type="text/css">
		#adminmenu div.separator { background-color: #666666 !important; }
		</style>';

}
add_action('admin_head', 'customAdmin');

// Body class for browsers
add_filter('body_class','myfunction_browser_body_class');
function myfunction_browser_body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

	if($is_lynx) $classes[] = 'lynx';
	elseif($is_gecko) $classes[] = 'gecko';
	elseif($is_opera) $classes[] = 'opera';
	elseif($is_NS4) $classes[] = 'ns4';
	elseif($is_safari) $classes[] = 'safari';
	elseif($is_chrome) $classes[] = 'chrome';
	elseif($is_IE) $classes[] = 'ie';
	else $classes[] = 'unknown_browser';

	if($is_iphone) $classes[] = 'iphone';
	return $classes;
}

// cache la boite de mise à jour pour user sauf l'ID 1 (normalement Megavolt)
$uID = array(1,2);
if ( ! in_array(get_current_user_id(), $uID) ) {
	add_action('admin_init', create_function(false,"remove_action('admin_notices', 'update_nag', 3);"));
	add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
	add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
}


// load les CSS spécifique au site du client
if ( ! function_exists( 'load_my_style_default' ) ):
	function load_my_style_default() {

		/* CSS */
		wp_enqueue_style('css_site', get_stylesheet_directory_uri() . '/css/style.min.css', '', '1.0.0', 'all' );
		wp_enqueue_style('googleFonts', 'http://fonts.googleapis.com/css?family=Roboto:400,300,700,500' );

		/* SCRIPT */
		// IMPORTANT !!!! si vous utilisez wooCommerce, il faut utiliser le JQUERY inclus dans wordpress, commentez « wp_deregister_script('jquery') et le JS 1.10.1
		// IMPORTANT !!!! le jquery doit etre chargé localement à cause de l'AJAX de gravity form
		wp_deregister_script('jquery');
		//wp_enqueue_script('js_jquery', 'http://code.jquery.com/jquery-1.10.1.min.js', '','1.10.1', false);
		// IMPORTANT !!!!

		wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/script.min.js', '','1.0', false);
		wp_enqueue_script('jquery.cookie', get_stylesheet_directory_uri() . '/js/jquery.cookie.js', '','1.4.1', false);
	}
	add_action('wp_enqueue_scripts', 'load_my_style_default');
endif;



// PERSONNALISER FORMAT IMAGE
// nom, width, height (9999 est la valeur proportionnelle), cropped ou non ( si on ne met pas ce parametre, les dimensions seront considérés comme max-width / max-height )
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'carrousel-home', 1800, 9999, false );
	add_image_size( 'header-page', 1800, 465, true );
	//add_image_size( 'galerie-thumb', 600 , 400 , true );
	add_image_size( 'galerie-thumb', 600 , 9999 , false);
	add_image_size( 'square-thumb', 768 , 768 , true );
	add_image_size( 'map-thumb', 150 , 80 , true );
}


add_action('init', 'mxo_config');
function mxo_config() {
	// ajoute la boite « Extrait » dans les pages
	add_post_type_support( 'page', 'excerpt' );												
	
	// active l'image à la une
	add_theme_support( 'post-thumbnails', array( 'post', 'page', 'cpt_attraits' ) );		

	// taille de l'image à la une
	add_image_size( 'post-thumb', 275 );												

	register_nav_menus( 
		array(
			'primary' => 'Navigation primaire'
		)
	);
}


/* Pagination */
if( !function_exists( 'the_pagination' ) ) {
	function the_pagination() {
		global $wp_query, $wp_rewrite;
		$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
		$pagination = array(
			'base' => @add_query_arg('page','%#%'),
			'format' => '',
			'total' => $wp_query->max_num_pages,
			'current' => $current,
			'show_all' => false,
			'end_size' => 1,
			'mid_size' => 2,
			'type' => 'list',
			'next_text' => '<i class="fa fa-angle-right"></i>',
			'prev_text' => '<i class="fa fa-angle-left"></i>'
		);
		
		if( $wp_rewrite->using_permalinks() )
			$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
		if( !empty($wp_query->query_vars['s']) )
		$pagination['add_args'] = array( 's' => str_replace( ' ' , '+', get_query_var( 's' ) ) );
		echo str_replace('page/1/','', paginate_links( $pagination ) );
	}	
}


// extrait d'un ACF
function acf_excerpt($title, $acf_id, $acf_excerpt_lenght) {
	global $post;
	$text = get_field($title, $acf_id);
	if ( '' != $text ) {
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$excerpt_length = $acf_excerpt_lenght; // x words
		$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('the_excerpt', $text);
}

function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
	return $excerpt;
}

function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
		array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	}
	$content = preg_replace('/[.+]/','', $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}


/* CODEX : There is no is_subpage() function yet */
function is_subpage($the_id){
	global $post;
	if ($post->post_parent == $the_id) {
		return true;
	} else {
		return false;
	}
}


// GravityForm - active le champ « Mot de passe »
add_action("gform_enable_password_field", "enable_password_field");
function enable_password_field($is_enabled){
	return true;
}

// GravityForm - scroll to confirm after submit
add_filter("gform_confirmation_anchor", create_function("","return true;"));


// SEB G.: SHOW PAGES AND POST IDs ON WORDPRESS CMS OVERVIEW
add_filter('manage_posts_columns', 'posts_columns_id', 5);
add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);
add_filter('manage_pages_columns', 'posts_columns_id', 5);
add_action('manage_pages_custom_column', 'posts_custom_id_columns', 5, 2);
function posts_columns_id($defaults){
	$defaults['wps_post_id'] = __('ID');
	return $defaults;
}
function posts_custom_id_columns($column_name, $id){
	if($column_name === 'wps_post_id'){
		echo $id;
	}
}

//Page options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}



// Move Yoast to bottom
add_filter( 'wpseo_metabox_prio', 'yoast_to_bottom' );
function yoast_to_bottom() {
	return 'low';
}


/*-------------------------------------------------------------------------------------------*/
/* cpt_attraits Post Type */
/*-------------------------------------------------------------------------------------------*/
class cpt_attraits {
	
	function cpt_attraits() {
		add_action('init',array($this,'create_post_type'));
	}
	
	function create_post_type() {
		$labels = array(
			'name'					=> 'Attraits',
			'singular_name'			=> 'Attrait',
			'add_new'				=> 'Ajouter un attrait',
			'all_items'				=> 'Tous les attraits',
			'add_new_item'			=> 'Ajouter un attrait',
			'edit_item'				=> 'Modifier l\'attrait',
			'new_item'				=> 'Ajouter un attrait',
			'view_item'				=> 'Voir l\'attrait',
			'search_items'			=> 'Rechercher un attrait',
			'not_found'				=> 'Aucun attrait trouv&eacute;',
			'not_found_in_trash'    => 'Aucune attrait dans la corbeille',
			'menu_name'				=> 'Attraits'

		);
		$args = array(
			'labels' 				=> $labels,
			'description' 			=> "",
			'public' 				=> true,
			'exclude_from_search' 	=> false,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true, 
			'show_in_nav_menus' 	=> true, 
			'show_in_menu' 			=> true,
			'show_in_admin_bar' 	=> true,
			'menu_position' 		=> 5,
			'menu_icon' 			=> 'dashicons-location',
			'capability_type' 		=> 'page',
			'hierarchical' 			=> true,
			'supports' 				=> array('title','editor','excerpt', 'thumbnail', 'revisions'),
			'has_archive' 			=> true,
			'rewrite' 				=> array('slug' => 'attraits'),
			'query_var' 			=> true,
			'can_export' 			=> true
		); 
		register_post_type('cpt_attraits',$args);
		flush_rewrite_rules();
	}
}

$cpt_attraits = new cpt_attraits();

function taxo_attraits() {
	$labels = array(
		'name'              => _x( 'Types', 'mxo' ),
		'singular_name'     => _x( 'Types', 'mxo' ),
		'search_items'      => __( 'Rechercher un type' ),
		'all_items'         => __( 'Toutes les types' ),
		'parent_item'       => __( 'Type parent' ),
		'parent_item_colon' => __( 'Type parent:' ),
		'edit_item'         => __( 'Modifier le type' ), 
		'update_item'       => __( 'Mettre au type' ),
		'add_new_item'      => __( 'Ajouter un type' ),
		'new_item_name'     => __( 'Ajouter un type' ),
		'menu_name'         => __( 'Types' ),
	);
	$args = array(
		'labels' 			=> $labels,
		'hierarchical' 		=> true,
		'rewrite' 			=> array('slug' => 'type-attrait'),
		'query_var' 		=> true,
	);
	register_taxonomy( 'taxo_attraits', 'cpt_attraits', $args );
}
add_action( 'init', 'taxo_attraits', 0 );



add_action( 'pre_get_posts', function ( $query ) 
{
    if ( !is_admin() && $query->is_main_query() ) {
    	if ($query->is_post_type_archive() || $query->is_tax()) {
	     	$post_type = get_query_var('post_type','');
            $query->set('order','ASC');
            $query->set('orderby','title');
            $query->set( 'posts_per_page', '12');
	    }
	    elseif ($query->is_search()) {
	    	$query->set('post_type', array('cpt_attraits', 'page', 'post' ));
	    }
    } 
       
});

?>